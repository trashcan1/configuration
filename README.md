# T-Mobile Trash Can Custom Configuration
This is a combination of T-Mobile Nokia scripts and finds in an effort to make static ip like performance available for more people. Not to be used for malicious activity.

Also, change the security vulerabilities!

## Finds so far
Links will be posted with notable notes below, if no notes are posted it hasn't been done yet. All repos, if they can be in this "group" of projects is mirrored if it can be.
[Trashcan Group](https://gitlab.com/trashcan1)

### Software

[Serving Web Services Behind CGNAT with Cloudflare Tunnel](https://eddiez.me/working-around-cgnat/)

[Hacking the Nokia Fastmile: Part 1](https://eddiez.me/hacking-the-nokia-fastmile/)

[Hacking the Nokia Fastmile: Part 2](https://eddiez.me/hacking-the-nokia-fastmile-pt2/)

[Hacking the Nokia Fastmile: Part 3](https://eddiez.me/hacking-the-nokia-fastmile-pt3/)

#### Source Code
[thedroidgeek/nokia-router-cfg-tool.py](https://gist.github.com/thedroidgeek/80c379aa43b71015d71da130f85a435a)

[EricTerrell/TrashCanMonitor](https://github.com/EricTerrell/TrashCanMonitor)

##### APIs or RAW info
[5 endpoints](https://www.reddit.com/r/tmobileisp/comments/lqpc0l/comment/gplb7td/?utm_source=share&utm_medium=web3x)

### Mods

[External Antenna Mod](https://www.waveform.com/a/b/guides/hotspots/t-mobile-5g-gateway-nokia)
[]()
[]()
[]()
[]()

### Threads / Forums

[Nokia Fastmile 5G hacking thread](https://forums.whirlpool.net.au/archive/3qqq56y3)

[Reddit Thread by engage16 called Trashcan Hacking](https://www.reddit.com/r/tmobileisp/comments/qxxent/trashcan_hacking/)
router IP is 192.168.12.1 but the MODEM IP is 192.0.0.1
web interface can be accessed via the link www.webgui.nokiawifi.com
‘superadmin’ access to the WebGUI, this has debug abilities

[READ If You Are New and Having Issues](https://www.reddit.com/r/tmobileisp/comments/n7pdnz/read_if_you_are_new_and_having_issues/) 
Potentially set dns to [1.1.1.1](https://one.one.one.one/) for better dns resolution. [DNS](https://www.reddit.com/r/tmobileisp/comments/n7pdnz/comment/gxejgze/?utm_source=share&utm_medium=web3x) 

[How to use your own router with T-Mobile Home Internet](https://www.reddit.com/r/tmobileisp/comments/m88kd2/how_to_use_your_own_router_with_tmobile_home/)

https://bostonenginerd.com/posts/tmobile-isp/

https://www.reddit.com/r/tmobileisp/comments/luslbf/comment/gpuuim4/

### Random Other Linkes
[Home Assistant](https://www.home-assistant.io/)


